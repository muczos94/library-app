﻿using Register.Business.Layer.Dto;

namespace Register.Business.Layer.Interfaces
{
    public interface IRaportService
    {
        int CheckHowManyParcent(int maxPoints, int points);
        string CheckIfResultsIsHigherThanThreshold(int parcent, int threshold);
        void ExportRaportToFile(string msg, ReportDto reportDto);
    }
}