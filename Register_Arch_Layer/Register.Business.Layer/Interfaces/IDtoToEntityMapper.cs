﻿using Register.Business.Layer.Dto;
using Register.Data.Layer.Models;

namespace Register.Business.Layer.Interfaces
{
    public interface IDtoToEntityMapper
    {
        Homework HomeworkDtoToModelEntity(HomeworkDto homeworkDto);
    }
}