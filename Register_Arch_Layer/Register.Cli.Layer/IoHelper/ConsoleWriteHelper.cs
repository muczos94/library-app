﻿using System;

namespace Register.Cli.Layer.IoHelper
{
    class ConsoleWriteHelper
    {
       

        public static void PrintOperationSuccessMessage(bool success)
        {
	        Console.WriteLine(success ? "Operacja powiodła sie!" : "Cos poszlo nie tak..");
        }

        public static void CommandListText()
        {
            Console.WriteLine("|------------------DZIENNIK ZAJEĆ------------------|\n" +
                              "\t\t\t\tmade by Jakub Gerlee \n" +
                              "\nSelectCourse - Wybierz kurs" +
                              "\nAddNewCourse - Stworz kurs" +
                              "\nAddNewStudent - Dodanie nowego studenta" +
                              "\nAddNewDay - Dodanie dnia kursu" +
                              "\nAddNewHomework - Dodanie nowej pracy domowej" +
                              "\nCourseRaport - Raport kursu" +
                              "\nEditStudent - Edycja danych studenta" +
                              "\nEditCourse - Edycja danych kursu" +
                              "\nExit - Wyjscie z aplikacji\n\n");
        }
    }
}
