﻿using Register.Data.Layer.Models;

namespace Register.Data.Layer.Interfaces
{
    public interface IStudentRepoService
    {
        Student GetStudentByPesel(long pesel);
        bool ChangeStudentPersonalData(Student student);
    }
}