using Register.Data.Layer.Models;

namespace Register.Data.Layer.Interfaces
{
    public interface ICourseDayRepoService
    {
        CourseDay GetCourseDayFromD(int idStudent, int idCourse);
        bool AddNewCourseDay(CourseDay courseDayToData);
    }
}